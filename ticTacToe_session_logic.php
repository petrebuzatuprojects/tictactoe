<?php
session_start();

if(!isset($_SESSION['table']) || isset($_GET['reset'])) {
    $_SESSION['table'] = [
        [false, false, false],
        [false, false, false],
        [false, false, false]
    ];
    $_SESSION['Player']='X';
    $_SESSION['isWinner']=false;
}
if (isset ($_GET['indexI']) && isset ($_GET['indexJ'])){

    if ($_SESSION['table'][$_GET['indexI']][$_GET['indexJ']] === false && $_SESSION['isWinner']===false )
    {
        $_SESSION['table'][$_GET['indexI']][$_GET['indexJ']] = $_SESSION['Player'];
    }
    if ($_SESSION['Player']=='X')
    {
        $_SESSION['Player']='O';
    }
    else
    {
        $_SESSION['Player']='X';
    }
}

$table = $_SESSION['table'];
$Player = $_SESSION['Player'];



include_once 'ticTacToe_logic.php'; //verificare castigator
include_once 'afisareHTML.php';

if($_SESSION['isWinner']): ?>
    <a href="ticTacToe_session_logic.php?reset=1">Reset</a>
<?php endif; ?>